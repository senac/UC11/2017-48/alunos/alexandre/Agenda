package com.example.sala304b.agenda2018;

import android.graphics.Bitmap;

public class Agenda {

    private String id;
    private String nome;
    private String endereco;
    private String telefone;
    private String celular;
    private transient Bitmap foto;



    public Agenda() {
    }

    public Agenda(String nome, String endereco, String telefone, String celular, Bitmap foto) {
        this.nome = nome;
        this.endereco = endereco;
        this.telefone = telefone;
        this.celular = celular;
        this.foto = foto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Bitmap getFoto() {
        return foto;
    }

    public void setFoto(Bitmap foto) {
        this.foto = foto;
    }
}