package com.example.sala304b.agenda2018;

import android.arch.lifecycle.Lifecycle;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    @Override
    public boolean OnCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater() ;
        inflater.inflate(R.menu.menu, menu );
        return true;

}

    public void Novo (MenuItem Item) {
        Intent intent = new Intent(this, NovoActivity.class);
        startActivity(intent);
    }
    public void Ajuda (MenuItem Item) {
        Intent intent = new Intent(this, AjudaActivity.class);
        startActivity(intent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }
}