package com.example.sala304b.agenda2018;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

/**
 * Created by sala304b on 05/03/2018.
 */

public class bancodedadosagenda extends SQLiteOpenHelper {

    private static final String DATABASE = "Agenda";
    private static final int VERSAO = 1;

    public bancodedadosagenda(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String dd1 = "CREATE TABLE Agenda( " +
                "nome TEXT NOT NULL, " +
                "endereco TEXT, " +
                "telefone TEXT, " +
                "celular TEXT);";
        db.execSQL(dd1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String dd1 = "DROP TABLE IF EXISTS Agenda";
        db.execSQL(dd1);
        this.onCreate(db);
    }

    public void salvar(Agenda agenda) {
        ContentValues values = new ContentValues();
        values.put("nome", agenda.getNome());
        values.put("endereco", agenda.getEndereco());
        values.put("telefone", agenda.getTelefone());
        values.put("celular", agenda.getCelular());
        values.put("foto", agenda.getFoto().toString());

        getWritableDatabase().insert("Agenda", null, values);


    }

    public List<Agenda> getLista(){
        List<Agenda> lista = new ArrayList<>();
        String colunas[] = {"nome", "endereco", "telefone", "celular", "foto"};
        Cursor cursor = getWritableDatabase().query("Agenda", colunas, null, null, null, null, null);
        while (cursor.moveToNext()) {

        }
        return lista;
    }
}