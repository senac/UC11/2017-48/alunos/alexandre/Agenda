package com.example.sala304b.agenda2018;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class NovoActivity extends AppCompatActivity {



    private EditText nomedotexto;
    private EditText enderecodotexto;
    private EditText telefonedotexto;
    private EditText celulardotexto;
    private Button Salvar;
    private ImageView imageView;
    private Agenda agenda ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);
        nomedotexto = findViewById(R.id.nomedotexto);
        telefonedotexto = findViewById(R.id.enderecodotexto);
        enderecodotexto = findViewById(R.id.telefonedotexto);
        celulardotexto = findViewById(R.id.celulardotexto);
        Salvar = findViewById(R.id.Salvar);

        Salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agenda = new Agenda();
                agenda.setNome(nomedotexto.getText().toString());
                agenda.setEndereco(enderecodotexto.getText().toString());
                agenda.setTelefone(telefonedotexto.getText().toString());
                agenda.setCelular(celulardotexto.getText().toString());
                agenda.setFoto(imageView.setImageBitmap());


                try {

                }
            }
        });

    }
}

