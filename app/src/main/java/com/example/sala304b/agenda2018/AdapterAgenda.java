package com.example.sala304b.agenda2018;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sala304b on 05/03/2018.
 */

public class AdapterAgenda extends BaseAdapter {

    private List<Agenda> lista;
    private Activity contexto;

    public AdapterAgenda(List<Agenda> lista, Activity contexto) {
        this.lista = lista;
        this.contexto = contexto;
    }


    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int indice) {
        return this.lista.get(indice);
    }

    @Override
    public long getItemId(int id) {
        int posicao = 0;
        for (int i = 0; i < this.lista.size(); i++) {
           if ((this.lista.get(i).getId() == id) {
               posicao = i;
               break;
            }
        }

        return posicao;
    }

    @Override
    public View getView(int posicao, View convertView, ViewGroup  parent) {
        View view = contexto.getLayoutInflater().inflate(R.layout.listaagenda,parent, false);

        TextView textViewNome = view.findViewById(R.id.nomedotexto);

        Agenda agenda = this.lista.get(posicao);
        textViewNome.setText(agenda.getNome());

        return view;

    }

}
